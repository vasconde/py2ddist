# des:    script para ajustamento de uma trilateracao.
#         sao determinados deslocamentos a partir de 
#         diferenças de observações (logaritmo das distancias)
# author: vasconde
# versao: v1 12/12/14

import math
from numpy import *

from deri import d_ln_dist

debug = True

# --- Dados --- #

## Caminhos para os ficheiros de entrada

# diferença de observações (Y2 - Y1) distancias
nome_dist_file = "./data3/ln_dist.txt"

if debug:
    nome_dist_file = "./data4/dist.txt"

# coordenadas aproximadas dos pontos da rede
nome_aprox_file = "./data4/coo.txt"

# pontos fixos
nome_fixos_file = "./data4/fixos.txt"

# --- Prog- --- #

## Programa principal

## funcoes

## calcula a distancia de a para b
def calc_dist_2 (a,b):
    return (b[0] - a[0])**2 + (b[1] - a[1])**2 

def calc_dist (a,b):
    return math.sqrt( calc_dist_2 (a,b) )

## recolha das observacoes

# recolhe as linhas do ficheiro (estacao,visado,ddist)
in_file = open(nome_dist_file, "rt")
linhas = [line.strip() for line in in_file] #lista de strings
in_file.close()
#print (pares_str)

# passa as linhas para listas de pares [estacao,visado,distancia]
dist_str = [linha.rsplit('|') for linha in linhas] #lista de pares (em listas)
#print(dist_str)

## recolha dos pontos fixos

# recolhe as linhas do ficheiro (estacao,visado,dist)
in_file = open(nome_fixos_file, "rt")
linhas = [line.strip() for line in in_file] #lista de strings
in_file.close()
#print (pares_str)

# passa as linhas para listas de pares [estacao,visado,distancia]
fixos = [linha.rsplit('|') for linha in linhas] #lista de pares (em listas)
print('fixos')
print(fixos)
print()

## recolha de coordenadas aproximadas

# recolhe as linhas do ficheiro (estacao,visado,dist)
in_file = open(nome_aprox_file, "rt")
linhas = [line.strip() for line in in_file] #lista de strings
in_file.close()
#print (pares_str)

# passa as linhas para listas de pares [estacao,visado,distancia]
aprox_str = [linha.rsplit('|') for linha in linhas] #lista de pares (em listas)
#print()
#print(aprox_str)

## coverter strings para double

aproxs = [[ap[0], float(ap[1]), float(ap[2])] for ap in aprox_str]
dists = [[el[0], el[1], float(el[2])] for el in dist_str]

print('coordenadas aproximadas')
print(aproxs)
print()
print('distancias observadas')
print(dists)
print()

## indices dos fixos
index_f = []
i = 0
for ap in aproxs:
    for f in fixos:
        if(ap[0] == f[0]):
            if(f[1] == 'XY'):
                index_f.append(i*2)
                index_f.append(i*2 + 1)
            elif(f[1] == 'X'):
                index_f.append(i*2)
            else:
                index_f.append(i*2 + 1)
    i=i+1

print('indices dos fixos')
print(index_f)
print()

## Cria vetor das coordenadas aproximadas

X = []
for aprox in aproxs:
    X.append([aprox[1]])
    X.append([aprox[2]])
X = matrix(X)

print('Coordenadas aproximadas (X)')
print(X)
print()

## Criar vetor de diferenca de observacoes

DY = [ [d[2]] for d in dists ] # dif dist observadas

if debug:
    DY = []
    for d in dists:
        # saca os idices dos dos pontos a e b
        ia = [ap[0] for ap in aproxs].index(d[0])
        ib = [ap[0] for ap in aproxs].index(d[1])
        # saca as coordenadas dos pontos a e b
        a = [float(X[ia*2]),float(X[ia*2+1])]
        b = [float(X[ib*2]),float(X[ib*2+1])]

        DY.append( [ d[2] / calc_dist (a,b) ] )


for i in index_f:     # deslocamento dos pontos fixos (zero)
    DY.append([0])

DY = matrix(DY)

print('diferenca de observacoes (DY)')
print(DY)

## Criar matriz de pesos (unitaria) MCSO

W = identity(size(DY))

i = -1
for ind in index_f:     # pesos para os pontos fixos
    W[i,i] = 100000000
    i = i - 1

print()
print('Matriz de pesos (W)')
print(W)


## Cria matriz de configuracao de primeira ordem MCPO

A = zeros([size(DY),size(X)])

i = 0
for dist in dists:
    # saca os idices dos dos pontos a e b
    ia = [a[0] for a in aproxs].index(dist[0])
    ib = [a[0] for a in aproxs].index(dist[1])
    
    # saca as coordenadas dos pontos a e b
    a = [float(X[ia*2]),float(X[ia*2+1])]
    b = [float(X[ib*2]),float(X[ib*2+1])]


    # determinas as derivadas parciais dos pontos a e b
    deri = d_ln_dist (a,b)

    # preenche a matriz A
    A[i,ia*2] = deri[0][0]
    A[i,ia*2+1] = deri[0][1]
    A[i,ib*2] = deri[1][0]
    A[i,ib*2+1] = deri[1][1]
    
    i = i+1
    
# linhas da MCPO correspondentes aos pontos fixos
for ind in index_f:
    A[i,ind] = 1

    i = i+1

#    print()
#    print('MCPO (A)')
#    print(A)

A = matrix(A) # forcar a ser uma matriz (numpy)

W = matrix(W) # forcar a ser uma matriz (numpy)

# Ajustamento - estimacao dos deslocamentos

N = A.T * W * A

DX = N.I*A.T*W*DY

print()
print('deslocamentos (DX)')
print(DX)

# determinacao dos residuos
v = A * DX + DY

print()
print('residuos (v)')
print(v)

# desvio padrao aposteriori

sM = v.T * W * v

df = size(DY) - size(DX)

print()
print('numero de graus de liberdade (df)')
print(df)

s = math.sqrt(float(sM)/df)

print()
print('devio padrao aposteriori (s)')
print(s)

print()
