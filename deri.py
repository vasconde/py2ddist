# des: derivadas para formulação das equações de observação
# aut: vasconde

import math

# Derivada da Distancia entre a e b
# entra: ponto a [xa,ya] e ponto b [xb,yb]
# sai: [d/dx_a, d/dy_a] [d/dx_b, d/dy_b]
def d_dist (a,b):
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    d_ab = math.sqrt(dx**2 + dy**2)

    d_dxa = -dx/d_ab
    d_dya = -dy/d_ab

    d_dxb = dx/d_ab
    d_dyb = dy/d_ab

    return [[d_dxa,d_dya],[d_dxb,d_dyb]]

# Derivada do Logaritmo da Distancia entre a e b
# entra: ponto a [xa,ya] e ponto b [xb,yb]
# sai: [d/dx_a, d/dy_a] [d/dx_b, d/dy_b]
def d_ln_dist (a,b):
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    d_ab = math.sqrt(dx**2 + dy**2)

    d_dxa = -dx/(d_ab**2)
    d_dya = -dy/(d_ab**2)

    d_dxb = dx/(d_ab**2)
    d_dyb = dy/(d_ab**2)

    return [[d_dxa,d_dya],[d_dxb,d_dyb]]
